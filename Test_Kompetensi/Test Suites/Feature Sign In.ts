<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Feature Sign In</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>2e03757e-93ba-4700-a1f0-4f3403c912bd</testSuiteGuid>
   <testCaseLink>
      <guid>9399c65f-547f-4da6-8e90-8cb9b4e36154</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Pages/Step Definition/Page_SignIn/Feature Sign In/SIG001 - User want to sign using correct credential</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>70cfa9e8-513b-42a9-ac9e-ce45a1b47087</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a9e3fb0b-0041-471c-ac6f-06bb013b11fb</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>a4095ac4-b2a2-4fb8-9c5c-5ace36bc4ed0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Pages/Step Definition/Page_SignIn/Feature Sign In/SIG002 - User want to sign in using Continue with Google</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>88d51e73-0d77-4a5c-a012-2a4f58c91df3</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>ce3647ce-0345-4d1b-b92a-110e113fd307</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Pages/Step Definition/Page_SignIn/Feature Sign In/SIG003 - User want to sign in using Continue with Facebook</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>f2639ed3-802f-4c0b-a2ef-7edcb74f5748</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Pages/Step Definition/Page_SignIn/Feature Sign In/SIG004 - User want to sign in using Continue with Apple</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>a825de70-b78a-4b1f-ab16-ab26d70350a9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Pages/Step Definition/Page_SignIn/Feature Sign In/SIG005 - User want to sign in using Continue with Number Phone</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ff23ad0b-ee43-4b47-8e5e-f05e2da687b3</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>3322071d-e03d-4eae-81bf-f8277156605a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Pages/Step Definition/Page_SignIn/Feature Sign In/SIG006 - User want to sign in without email and password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>22149b63-a168-4e83-9470-752d5802612e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Pages/Step Definition/Page_SignIn/Feature Sign In/SIG007 - User want to sign in only input registered email</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>4e1af64a-7a16-42f1-a5fd-d4fd5dc92e98</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Pages/Step Definition/Page_SignIn/Feature Sign In/SIG008 - User want to sign in only input registered password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>5422c151-1355-46b6-b1aa-6faac9f9141e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Pages/Step Definition/Page_SignIn/Feature Sign In/SIG009 - User want to sign in using valid email and invalid password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>21fac260-a93b-4e6e-ba1d-b5b07b635e03</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Pages/Step Definition/Page_SignIn/Feature Sign In/SIG010 - User want to sign in using invalid email and valid password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>9695ed2d-dae9-41a9-a5b2-4a1bf56b0dba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Pages/Step Definition/Page_SignIn/Feature Sign In/SIG011 - User want to sign in using invalid email and invalid password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
