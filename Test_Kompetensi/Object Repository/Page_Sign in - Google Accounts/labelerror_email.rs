<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>labelerror_email</name>
   <tag></tag>
   <elementGuidId>75895f8b-48ce-450f-a78a-306703e88c98</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='yDmH0d']/c-wiz/div/div[2]/div/div/div/form/span/section/div/div/div/div/div[2]/div[2]/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.o6cuMc.Jj6Lae</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>a74969ec-dd56-43f2-8f7f-6e695b924148</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>o6cuMc Jj6Lae</value>
      <webElementGuid>a8c553e0-12c0-4314-b571-117fdc906ab3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Enter an email or phone number</value>
      <webElementGuid>be950b19-161d-4b6d-ac6a-438777506f7a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;yDmH0d&quot;)/c-wiz[@class=&quot;g98c7c&quot;]/div[@class=&quot;aDGQwe&quot;]/div[@class=&quot;eKnrVb&quot;]/div[@class=&quot;CYBold&quot;]/div[@class=&quot;j663ec&quot;]/div[@class=&quot;tCpFMc&quot;]/form[1]/span[1]/section[@class=&quot;aTzEhb&quot;]/div[@class=&quot;CxRgyd&quot;]/div[@class=&quot;VBGMK&quot;]/div[@class=&quot;d2CFce cDSmF cxMOTc&quot;]/div[@class=&quot;rFrNMe N3Hzgf jjwyfe QBQrY zKHdkd sdJrJc Tyc9J k0tWj IYewr&quot;]/div[@class=&quot;LXRPh&quot;]/div[@class=&quot;dEOOab RxsGPe&quot;]/div[@class=&quot;o6cuMc Jj6Lae&quot;]</value>
      <webElementGuid>54d9be30-1329-4f3f-99e2-04e6803a5a4e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='yDmH0d']/c-wiz/div/div[2]/div/div/div/form/span/section/div/div/div/div/div[2]/div[2]/div</value>
      <webElementGuid>62836c66-e99c-474e-ac7a-e8d4c5674b66</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Spotify'])[1]/following::div[18]</value>
      <webElementGuid>35bac8d6-d065-4ebe-a2ef-b874282e00b1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Forgot email?'])[1]/preceding::div[2]</value>
      <webElementGuid>bebb8b4f-0ba5-463d-90c7-92b87aaa69aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='privacy policy'])[1]/preceding::div[21]</value>
      <webElementGuid>2f1cc1b2-1769-4f2f-9f03-800e2beabfde</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Enter an email or phone number']/parent::*</value>
      <webElementGuid>a121f93a-46a7-4097-b1ac-972e8daf0674</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div</value>
      <webElementGuid>fc4dd0e1-48a5-4fa1-8e57-3fcf6975f61c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Enter an email or phone number' or . = 'Enter an email or phone number')]</value>
      <webElementGuid>549606f8-31d0-4fda-a683-69f34ec06c44</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
