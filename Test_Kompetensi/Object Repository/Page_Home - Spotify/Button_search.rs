<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button_search</name>
   <tag></tag>
   <elementGuidId>b2d863b4-e082-44e0-a6ba-c973820c055d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[starts-with(@href, '/search')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[starts-with(@href, '/search')]</value>
      <webElementGuid>593d8401-f921-4334-ae4b-faa6babb6cc0</webElementGuid>
   </webElementProperties>
</WebElementEntity>
