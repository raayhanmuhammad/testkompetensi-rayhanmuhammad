<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>inputfield_nohp</name>
   <tag></tag>
   <elementGuidId>18a87f81-7cc0-4920-8daa-92f6070b3382</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='phonelogin-phonenumber']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#phonelogin-phonenumber</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>0d1b5032-d1c5-457a-9f28-5a4a03b27f7e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-invalid</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>e98ef165-b5fc-40c2-a399-720f4b670540</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>phonelogin-phonenumber</value>
      <webElementGuid>e7b0c07d-973e-4342-9660-c3de87c11b13</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>inputPhoneNumber</value>
      <webElementGuid>9a93d45d-63aa-40fc-b1b6-0c629980564a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>error-phone-number-non-digit error-phone-number-invalid</value>
      <webElementGuid>565b347e-9858-465a-9569-fb14f653d3b4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Phone number</value>
      <webElementGuid>67ae0a84-3060-4281-a160-ddd40e169f27</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>tel</value>
      <webElementGuid>df063f4d-db64-4ce3-bb77-5c87d040d15d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-testid</name>
      <type>Main</type>
      <value>phonelogin-phonenumber</value>
      <webElementGuid>743730c4-db39-4d4f-893d-c7d44cfc6358</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-encore-id</name>
      <type>Main</type>
      <value>formInput</value>
      <webElementGuid>ef6b8f9c-8bdc-4577-952e-cf4f562fa484</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>Input-sc-1gbx9xe-0 fOpTaL</value>
      <webElementGuid>5030a19d-ad29-417c-9bfa-9926ccd165a2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;phonelogin-phonenumber&quot;)</value>
      <webElementGuid>416d1e1a-5446-4249-b337-dc6784a43a93</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='phonelogin-phonenumber']</value>
      <webElementGuid>516f8b96-8dc6-4342-a6eb-bbc1100b2eb9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[2]/div/div/div/div[2]/input</value>
      <webElementGuid>b1709c83-be1d-4b0e-ae1e-f2b64a99aea3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>bd59ae57-d9e4-408c-84c0-38232ef8cb25</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'phonelogin-phonenumber' and @name = 'inputPhoneNumber' and @placeholder = 'Phone number' and @type = 'tel']</value>
      <webElementGuid>ed8317ac-9bc8-4602-bf99-9780fdf4a43f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
