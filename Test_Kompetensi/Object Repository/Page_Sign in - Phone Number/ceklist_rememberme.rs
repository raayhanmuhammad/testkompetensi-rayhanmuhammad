<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ceklist_rememberme</name>
   <tag></tag>
   <elementGuidId>fc21f581-49b2-4c97-b216-d88cf1f856ab</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div[2]/div/div/div[2]/div/div/label/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.Indicator-sc-1airx73-0.bpcByA</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>4b480260-6168-493e-a135-ff9e8402bda5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>Indicator-sc-1airx73-0 bpcByA</value>
      <webElementGuid>10268a7b-408e-45a4-8833-d1623708e2cc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;sc-AHaJN gYfGMA&quot;]/div[@class=&quot;sc-jfTVlA jjQlXd&quot;]/div[@class=&quot;sc-gScZFl hzDuNB&quot;]/div[@class=&quot;sc-gswNZR wKwWn&quot;]/div[@class=&quot;sc-hHTYSt fkfHMZ&quot;]/div[@class=&quot;Group-sc-u9bcx5-0 kWRxyd sc-bjfHbI gRAfve&quot;]/div[@class=&quot;Checkbox-sc-svpvf6-0 ktsNkz&quot;]/label[@class=&quot;Label-sc-cpoq-0 bbKAxy&quot;]/span[@class=&quot;Indicator-sc-1airx73-0 bpcByA&quot;]</value>
      <webElementGuid>dab05a05-71e1-4d59-8029-b96fc0579a5e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[2]/div/div/div[2]/div/div/label/span</value>
      <webElementGuid>3149c2e5-7ba1-443a-a524-af985a695b26</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span</value>
      <webElementGuid>e3d87bf9-b058-49f2-8a12-6b7fe0dfa72e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
